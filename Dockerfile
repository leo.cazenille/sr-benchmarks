FROM ubuntu
MAINTAINER leo.cazenille@gmail.com

RUN apt-get update
RUN apt-get upgrade -y
RUN DEBIAN_FRONTEND=noninteractive apt-get install -yq python3-pip git iputils-ping
RUN useradd -ms /bin/bash testuser

USER testuser
WORKDIR /home/testuser
RUN pip3 install numpy
RUN pip3 install deap
RUN pip3 install scoop
RUN git clone https://gitlab.com/leo.cazenille/sr-benchmarks.git
